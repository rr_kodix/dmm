@@include('modernizr-custom-3.0.0.js')

@@include('../../../../bower_components/svg4everybody/dist/svg4everybody.min.js')

@@include('../../../../bower_components/jquery/dist/jquery.min.js')

@@include('../../../../bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')

@@include('../../../../bower_components/chosen/chosen.jquery.min.js')

@@include('../../../../bower_components/underscore/underscore-min.js')

@@include('../../../../bower_components/backbone/backbone-min.js')

@@include('../../../../bower_components/jquery-ui/ui/datepicker.js')
@@include('../../../../bower_components/jquery-ui/ui/i18n/datepicker-ru.js')

@@include('../../../../bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js')



paceOptions = {
  startOnPageLoad: false,
  elements: false,
  restartOnPushState: false,
  restartOnRequestAfter: false
};

@@include('../../../../bower_components/PACE/pace.min.js')
//http://github.hubspot.com/pace/


@@include('../../../../bower_components/spin.js/spin.min.js')

@@include('../../../../bower_components/autoNumeric/autoNumeric.js')

@@include('../../../../bower_components/typeahead.js/dist/typeahead.bundle.min.js')

