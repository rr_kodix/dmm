class TableHead {

    constructor(options) {
        this.el = options.el;
        this.fixEl = options.fixEl;
        this.fixBlock = options.fixBlock;

        let self = this;

        
    }

    headCreate() {
        let elClone = this.el.html();
        this.fixEl.append(elClone);
    }

    colWidth() {
        let self = this;
        this.el.find('th').each(function(index, elem) {
            let elemWidth = $(this).width();
            let fixCol = self.fixEl.find('th');
            fixCol.eq(index).width(elemWidth);
        });
    }

    init() {
        this.headCreate();
        this.colWidth();

        let self = this;

        $(window).scroll(function() {
            let wScroll = $(window).scrollTop();
            let headPos = self.el.offset().top + 12;
            if (wScroll >= headPos) {
                self.headShow();
            } else {
                self.headHide();
            }
        });
    }

    destroy() {
        this.fixEl.empty();
    }

    reInit() {
        this.destroy();
        this.init();
    }

    headShow() {
        this.fixBlock.fadeIn(300);
    }

    headHide() {
        this.fixBlock.fadeOut(200);
    }

}

if ($('.js_table_header').length) {
    var tableHead = new TableHead({
        el: $('.js_table_header'),
        fixEl: $('.js_fixed_table_header'),
        fixBlock: $('.js_fixed_table_header').parents('.table_fix_head')
    });

    tableHead.init(); 
}


if ($('.js_table_header_sidebar').length) {
    var tableHeadSidebar = new TableHead({
        el: $('.js_table_header_sidebar'),
        fixEl: $('.js_fixed_table_header_sidebar'),
        fixBlock: $('.js_fixed_table_header_sidebar').parents('.table_fix_head')
    });

    tableHeadSidebar.headCreate();
    tableHeadSidebar.colWidth();
    tableHeadSidebar.headShow(); 

    $(document).on('click','.js_sidebar_pop',function(e){
        tableHeadSidebar.colWidth();
        e.preventDefault();
    });
}

