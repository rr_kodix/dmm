class SystemMsg {

    constructor(options) {

        this.msgTimer = true;
      
        $(document).on('click','.js_mgs_close',function(){
            var msgBlock = $(this).parents('.message_block');
            $(msgBlock).fadeOut(300, function(){
                $(msgBlock).remove();
            });
            clearTimeout(this.msgTimer);
        });
        
    }

    show(text = "сообщение", classname = false, delay = 0, icon = false, id="") {

        if(!$('.message_holder').length) {
            var msgHolder = document.createElement('div');
            msgHolder.className = "message_holder";
            $('body').append(msgHolder);
        } else {
            var msgHolder = document.querySelector('.message_holder');
        }

        var msgBlock = document.createElement('div');

        msgBlock.className = "message_block";

        if (id) {
            msgBlock.id = id;
        }

        msgBlock.innerHTML = '<div class="inside_wrapper"><div class="message_content"><span href="#" class="close js_mgs_close"><svg class="icon icon_delete"><use xlink:href="resources/img/sprite.svg#Delete"></svg></span><div class="message_text">'+`${text}`+'</div></div></div>';

        $(msgHolder).append(msgBlock);

        $(msgBlock).fadeIn(300);

        if (classname) {
            $(msgBlock).addClass(classname);
        }

        if (delay) {
            var self = this;
            this.msgTimer = setTimeout(function(){
                self.hideFade(msgBlock);
            }, delay * 1000);
        }

        if (icon) {
            $(msgBlock).find('.message_text').before('<svg class="icon icon_info"><use xlink:href="resources/img/sprite.svg#Info"></svg>');
        }
    }

    hideFade(elem) {
        $(elem).fadeOut(300, function(){
            $(elem).remove();
        });
    }

    hide(elem) {
        $(elem).hide();
        $(elem).remove();
    }

}



var systemMsg = new SystemMsg();

//примеры вызова
//systemMsg.show("Форма успешно сохранена", false, 3);
//systemMsg.show("Форма успешно сохранена", false, null, false, "hello");
//systemMsg.show("Форма заполнена неверно", "alarm", 3, true);
//systemMsg.show("<div>Есть похожие размещения в СМИ «www.mail.ru»: 04.11–30.11 сть похожие размещения в СМИ «www.mail.ru»: 04.11–30.11 сть похожие размещения в СМИ «www.mail.ru»: 04.11–30.11</div><div>Есть похожие размещения в СМИ «www.mail.ru»: 04.11–30.11</div>", "notice", 3, true);




