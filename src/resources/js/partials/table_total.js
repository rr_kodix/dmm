class FixTableTotal {

    constructor(options) {
        this.el = options.el;
        let self = this;
        $(window).scroll(function() {
            self.wScrollInit(self.elPos,self.elHeight,self.wHeight)
        });  
        $(window).resize(function() {
            self.reInit();
        });    
    }

    init() {
        this.elPos = this.el.offset().top; 
        this.elHeight = this.el.outerHeight();
        this.wHeight = $(window).height();
        this.wScrollInit(this.elPos,this.elHeight,this.wHeight);
    }

    reInit() {
        this.elFixRemove();
        this.init();
    }

    wScrollInit(elPos,elHeight,wHeight) {
        var wScroll = $(window).scrollTop() + wHeight - elHeight;
        if (wScroll <= elPos) {
            this.elFix()
        } else {
            this.elFixRemove();
        }
    }

    elFix() {
        this.el.addClass('fixed js_popup_show_margin');
    }

    elFixRemove() {
        this.el.removeClass('fixed js_popup_show_margin');
    }
}

if ($('.js_fix_total').length) {
    let fixTableTotal = new FixTableTotal({
        el: $('.js_fix_total'),
    });

    fixTableTotal.init();
}

//fixTableTotal.reInit();  //use to reinit the block position when update content


