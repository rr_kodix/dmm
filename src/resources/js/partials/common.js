//------------custom elements init------------


svg4everybody(); //initialize svg for IE


//custom scroll
function customScrollInit () {
    $(".js_custom_scroll").mCustomScrollbar({
        advanced:{
            updateOnContentResize: true
        }
    });
}

customScrollInit();


function customScrollHorizontalInit () {
    $(".js_custom_scroll_horizontal").mCustomScrollbar({
        axis:"x",
        advanced:{
            updateOnContentResize: true
        }
    });
}

customScrollHorizontalInit();
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


//custom select  

function customSelectInit () {
    $(".js_chosen_select").chosen({
        disable_search_threshold: 10,
        no_results_text: "Ничего не найдено",
        placeholder_text_single: " "
    });

    $('.js_chosen_select').on('chosen:showing_dropdown', function(e, params) {
        var el = params.chosen.container[0];
        if (!$(el).find('.js_custom_scroll').length) {
            $(el).find('.chosen-results').wrap('<div class="js_custom_scroll"></div>');
            customScrollInit();
        }
    });
}
customSelectInit();

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



// tooltips 
function tooltipInit () {
    $(".has_tooltip .tooltip").each(function(){
        var tooltip = $(this);
        var tooltipWidth = $(tooltip).outerWidth();
        tooltip.css({
          marginLeft: - ( tooltipWidth/ 2 ) + 'px'
        });
    });
}
tooltipInit();
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



//datepicker init
function datePicker() {
    $(".js_datepicker").each(function(){
        if (!$(this).hasClass('readonly')) {
            $(this).datepicker( $.datepicker.regional[ "ru" ] );
        }
    });
    //$(".js_datepicker").datepicker( $.datepicker.regional[ "ru" ] );

    $(document).on('click','.js_datepicker_show',function(){      
        if (!$(this).siblings(".js_datepicker").prop('disabled')&&!$(this).siblings(".js_datepicker").hasClass('readonly')) {
            $(this).siblings(".js_datepicker").datepicker( "show" );
        }
    });
}
datePicker();
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//$('.js_datepicker').datepicker('option', 'minDate', new Date(2016, 0, 20));



//dropdowns

//--prevent dropdown open
$(document).on('click','.js_dropdown_prevent',function(e){
    e.stopPropagation();
});



$(document).on('click','.js_dropdown_trigger',function(e){
    e.preventDefault();
    $('.js_dropdown_trigger').removeClass('opened');
    $('.js_dropdown_item').hide();
    $(this).toggleClass('opened');
    $(this).next('.js_dropdown_item').toggle();

});

//--hide dropdown on outer click
$(document).on('click',function(e){
    if( ($(e.target).closest('.js_dropdown_trigger').length==0)&&($(e.target).closest(".js_dropdown_item").length==0) ) {
        $('.js_dropdown_trigger').removeClass('opened');
        $('.js_dropdown_item').hide();
    }
});

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//menu toggle inside dropdown
$(document).on('click','.js_submenu_trigger',function(e){
    e.preventDefault();
    $(this).toggleClass('opened');
    $(this).next('.js_submenu_item').slideToggle(300);

});


//switcher
$(document).on('click','.js_switch',function(){
    $(this).addClass('active').siblings('.js_switch').removeClass('active');
});

$(document).on('click','.js_switch_content',function(e){
    var switchContent = $(this).attr('href');
    $(switchContent).addClass('active').siblings().removeClass('active');
    e.preventDefault();
});
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



//tabs
$(document).on('click','.js_tabs',function(e){
    $(this).addClass('active').siblings('.js_tabs').removeClass('active');
    e.preventDefault();
});
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


//check all checkboxes in table
$(document).on('change','.js_table_check_all',function(){
    if($(this).is(':checked')){
        $('.primary_table').find('.inp_checkbox input').prop('checked',true);
        $('.js_table_check_all').prop('checked',true);
    }
    else{
        $('.primary_table').find('.inp_checkbox input').removeAttr('checked');
        $('.js_table_check_all').removeAttr('checked');
    }
});
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~






// fixed elements on horisontal scroll

function fixedElemsCorrection(elems) {
    elems.each(function(){
        var elemLeft = $(window).scrollLeft();
        $(this).css({left:'-'+elemLeft+'px'});
    });
}

fixedElemsCorrection($('.js_gorizont_scroll'));

$(window).scroll(function(){
   fixedElemsCorrection($('.js_gorizont_scroll'));
});

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




// input mask
function maskedInput() {
    $(".js_date_mask").mask("99.99.9999");
    $(".js_phone_mask").mask("(999) 999-99-99");
}
maskedInput();
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



// preloader
//-- to call preloader use:
//-- Pace.restart();


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



// disabled input toggle in form
$(document).on('change','.js_disabled_input_toggler',function(){
    if($(this).is(':checked')){
        $(this).parents('.field_elem').find('.js_disabled_input').removeAttr('disabled');
    }
    else{
        $(this).parents('.field_elem').find('.js_disabled_input').prop('disabled',true);
    }
});
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




// spinner on file preview loading
function spinner() {
   var opts = {
     lines: 13, 
     length: 0, 
     width: 3, 
     radius: 10, 
     color: '#2274ac', 
     className: 'spinner',
     top: '50%',
     left: '0' // Left position relative to parent
   };
   var spinner = new Spinner(opts).spin();
   $('.spinner').html($(spinner.el)); 
}
spinner();
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




// equal height of file preview title
function eq_height() {
   $('.js_eq_height_item').height('auto');

    $('.js_eq_height').each(function(){
        var common_h = 0;
        $(this).find('.js_eq_height_item').each(function(){
            var this_h = $(this).height();
            if (this_h > common_h) {
                common_h = this_h;
            }
        });
        $(this).find('.js_eq_height_item').height(common_h);
    })   
}

eq_height();  //init each time when load new file

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~






//accordion
$(document).on('click','.js_accor_toggler', function(){
    $(this).next('.js_accor_toggler_content').slideToggle(300);
    $(this).toggleClass('opened');
});
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~








//autoNumeric
function numeric() {
    $('.js_numeric').autoNumeric('init',{
        aSep: ' ',
        aDec: ',',
        vMin: '0.00',
        vMax: '999999999999999999999999999.99'
    });
}
numeric();

//numeric without numbers after comma
function numericNoComma() {
    $('.js_numeric_nocomma').autoNumeric('init',{
        aSep: ' ',
        aPad: false,
        mDec: '0',
        vMin: '0.00',
        vMax: '999999999999999999999999999'
    });
}
numericNoComma();
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



//check days in form
function checkDays(data,item) {
    $(item).parents('.js_check_days_wrap').find('input').removeAttr('checked');
    if (data == 'weekends') {
        $(item).parents('.js_check_days_wrap').find('input.js_weekends').prop('checked',true);
    } if (data == 'workdays') {
        $(item).parents('.js_check_days_wrap').find('input.js_workdays').prop('checked',true);
    } if (data == 'all') {
        $(item).parents('.js_check_days_wrap').find('input').prop('checked',true);
    }
}

$(document).on('click','.js_check_days',function(e){
    var thisData = $(this).attr('data-check');
    $(this).siblings().removeClass('active');
    if (thisData != 'clear') {
        $(this).addClass('active');
    }
    checkDays(thisData, $(this));
    e.preventDefault();
});
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



//check days in form
//   http://twitter.github.io/typeahead.js/examples/
//   https://github.com/twitter/typeahead.js/blob/master/doc/bloodhound.md
function autoCompleteInit() {

    $('.js_autocomplete').each(function(){

        var self = $(this),
            dataUrl = self.attr('data-url'),
            data = new Bloodhound(self.attr('data-remote') ? {
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: dataUrl,
                remote: {
                  url: dataUrl + '?query=%QUERY',
                  wildcard: '%QUERY'
                }
            } : {
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: dataUrl            
            });

        self.typeahead({
            minLength: 1,
            highlight: true,
            hint: true
        },
        {
            name: 'data',
            source: data
        });
    });
   
}
autoCompleteInit();
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


//table toggler
$(document).on('click','.js_table_toggler', function(){
    $(this).toggleClass('opened');

    $(this).parents('tr').nextAll('tr').each(function(){
        if ($(this).hasClass("row_trigger")) return false;
        $(this).toggle();
    });

    tooltipInit();
});
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



//select dropdown
$(document).on('click','.js_select_drop',function(e){
    if (!$(this).hasClass('disabled')) {
        $(this).addClass('focused');
        $(this).parent().find('.js_select_drop_item').toggle();
    }
    e.preventDefault();
});
//--hide dropdown on outer click
$(document).on('click',function(e){
    if( ($(e.target).closest('.js_select_drop_item').length==0)&&($(e.target).closest(".js_select_drop").length==0) ) {
        $('.js_select_drop_item').hide();
        $('.js_select_drop').removeClass('focused');
    }
});

//select dropdown

//checkboxes select dropdown
$(document).on('change','.js_check_select input',function(){
    var data = $(this).val();
    
    var dataArr = [];
    var allData = $(this).parents('.js_check_select').find('input:checked').each(function() {
        var data = $(this).is('[data-title]') ? $(this).data('title') : $(this).val();
        dataArr.push(data);
    });

    var arrStr = dataArr.join(', ');

    $(this).parents('.js_check_select').prev('.js_check_select_result').find('.result_wrap').html(arrStr);
});



function totalTableColsWidth() {
    let elem = $('.js_total_table');
    let elemWidthSource = $('.js_table_header');

    elem.find('.js_td_width').each(function(index, tdElem) {

        var sourceW = elemWidthSource.find('.js_td_width_source').eq(index).width();
        $(tdElem).width(sourceW);
    });
}
totalTableColsWidth();


function calculateHeaderColWidth() {
    $('.detached-header').each( (i, detachedHeader) => {
        let $limitedTable = $(detachedHeader).siblings('.limited-table-x').find('table');

        $(detachedHeader).find('td, th').each( (i, elem) => {
            let width = $limitedTable.find('td').eq($(elem).index()).outerWidth();
            console.log(width);
            $(elem).width(width);
        });
    });
}

function calculateTableMaxHeight(visibleCount = 10) {
    $('.limited-table-x').each( (i, elem) => {

        let $trs = $(elem).find('tr');

        if ($trs.length <= visibleCount) {
            $(elem).css('max-height', 'auto');
            return;
        }

        let height = 0;
        $(elem).find('tr').slice(0, visibleCount).each( (i,elem) => {
            height += $(elem).height();
        });
        $(elem).css('max-height', `${height}px`);
    });

    calculateHeaderColWidth();

    customScrollInit();
}

calculateTableMaxHeight();