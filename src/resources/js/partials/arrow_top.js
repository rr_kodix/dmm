//arrow btn -> body scrolling top
class ArrowTopScroll {

    constructor(options) {
        this.el = options.el;
        this.contentEl = options.contentEl;
        let self = this;

        this.el.click(function() {
            $('html,body').animate( { scrollTop: 0 }, 400 );
        });

        //correction when window resize
        $(window).resize(function(){
            self.elPos();
        });

        //correction when horisontal scroll
        $(window).scroll(function(){
            let windowLeft = $(window).scrollLeft();
            self.elPosLeft(windowLeft);

            let windowScroll = $(window).scrollTop();
            let arrowShowPos = 100;
            if (windowScroll >= arrowShowPos) {
                self.elShow();
            } else {
                self.elHide();
            }
        });

    }

    elPosLeft(windowLeft) {
        this.el.css({'margin-left':'-'+windowLeft+'px'});
    }

    elPos() {
        let bodyW = $('body').width();
        let contentW = this.contentEl.width();
        let selfPos = bodyW / 2 + contentW / 2 + 40;
        this.el.css('left',selfPos);
    }

    elShow() {
        this.el.fadeIn(300);
        this.elPos();
    }

    elHide() {
        this.el.fadeOut(300);
    }


}

let arrowTopScroll = new ArrowTopScroll({
    el: $('.js_scroll_top_btn'),
    contentEl: $('.inside_wrapper')
});

