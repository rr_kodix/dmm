$('body').append('<div class="fix_nav">'+
    '<div class="title"><span>навигация</span></div>'+
    '<div class="hidden_nav">'+
        '<div class="hidden_nav_col">'+
        
            'заявки:<ul>'+
            '<li><a href="index.html">список</a></li>'+
            '<li><a href="index--no-result.html">список(пустой)</a></li>'+
            '<li><a href="apply.html">детальная</a></li>'+
            '<li><a href="apply_msg.html">сообщения</a></li>'+
            '<li><a href="apply_history.html">история</a></li>'+
            '<li><a href="apply_status.html">статусы</a></li>'+
            '<li><a href="apply_reports.html">отчеты</a></li>'+
            '<li><a href="apply_btl.html">BTL</a></li>'+
            '<li><a href="apply_btl_reports.html">BTL: отчетность</a></li>'+
            '<li><a href="apply_btl_compens.html">BTL: компенсация</a></li>'+
            '</ul>'+

            'групповые заявки:<ul>'+
            '<li><a href="group_apply.html">форма</a></li>'+
            '<li><a href="group_apply_press.html">пресса</a></li>'+
            '<li><a href="group_apply_radio.html">радио</a></li>'+
            '</ul>'+

        '</div>'+

        '<div class="hidden_nav_col">'+

            'авторизация:<ul>'+
            '<li><a href="login.html">логин</a></li>'+
            '<li><a href="registration.html">регистрация</a></li>'+
            '<li><a href="reg_success.html">регистрация успешно</a></li>'+
            '<li><a href="pass_restore.html">восстановление пароля</a></li>'+
            '</ul>'+

            'годовое планирование:<ul>'+
            '<li><a href="year_plan.html">список</a></li>'+
            '<li><a href="year_plan_detail.html">детальная</a></li>'+
            '<li><a href="year_plan_statistic.html">отчеты и статистика</a></li>'+
            '</ul>'+

            'квартальная отчетность:<ul>'+
            '<li><a href="quarterly_report.html">отчет</a></li>'+
            '</ul>'+

            'системные:<ul>'+
            '<li><a href="404.html">страница 404</a></li>'+
            '<li><a href="layout.html">layout page</a></li>'+
            '</ul>'+

        '</div>'+
    '</div></div>');


$('.fix_nav .title').click(function(){
    $('.fix_nav .hidden_nav').slideToggle(300);
});


if ( $('.popup').hasClass('verstka_show') ) {
    $('.popup').addClass('show');
    popupScrollHeight($('.popup'));
    bodyStopScroll($('.popup'));
}


$('.show_preloader').click(function(){
    Pace.restart();
});



$('.show_msg').click(function(){
    systemMsg.show("Форма успешно сохранена", false, 3);
});

$('.show_msg_nothide').click(function(){
    systemMsg.show("Форма успешно сохранена", false, null, false, "hello");
});

$('.show_msg_alarm').click(function(){
    systemMsg.show("Форма заполнена неверно", "alarm", 3, true);
});

$('.show_msg_notice').click(function(){
    systemMsg.show("<div>Есть похожие размещения в СМИ «www.mail.ru»: 04.11–30.11 сть похожие размещения в СМИ «www.mail.ru»: 04.11–30.11 сть похожие размещения в СМИ «www.mail.ru»: 04.11–30.11</div><div>Есть похожие размещения в СМИ «www.mail.ru»: 04.11–30.11</div>", "notice", 3, true);
});



// $(document).on('click','[href="#"]',function(e){
//     e.preventDefault();
// });